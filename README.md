# Resumen

* En este trabajo se presenta lo poco que he podido realizar en tan poco tiempo y asuntos familiares y laborales, cabe resaltar que puede tener varios fallos y malas practicas como no usar archivos de configuraciones ya que me enfoque en hacerlo funcionar lo mejor posible y no le di prioridad a los detalles.

  Funcionalidades interesantes que podrían ser realizadas usando el api de twitter:

  * el cliente podría solicitar su estatus de facturas o incluso su balance movil.
  * se podría realizar recargas usando el numero de alguna tarjeta prepagada.
  * consultar su balance da datos.
  * próximo ciclo de facturación.
  * presentar noticias o promociones de forma automática.



Flujos de Mule 4.3 [studio-workspace](studio-workspace) 

Proceso de envió ultima factura utilizando los servicios desarrollados en mule [cronJob](cronJob) 

Documentación de todo lo desarrollado [apiDOc](apiDOc) 


# Resume

* In this work the little that I have been able to do in such a short time and family and work matters is presented, it should be noted that it may have several failures and bad practices such as not using configuration files since I focus on making it work as best as possible and not I gave priority to the details.

   Interesting features that could be done using the twitter api:

   * The client could request his invoice status or even his mobile balance.
   * You could make recharges using the number of a prepaid card.
   * check your balance gives data.
   * next billing cycle.
   * present news or promotions automatically.



Mule 4.3 flows [studio-workspace](studio-workspace)

Last invoice sending process using the services developed in mule [cronJob](cronJob)

Documentation of everything developed [apiDOc](apiDOc) 
