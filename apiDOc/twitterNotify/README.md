# cron_job
Run the python script and generate stores the standard output in a log file


##### Connections Diagram
```mermaid
graph TD
	id0((cron_job)) --execute--> twitter.sh
	id1{{twitter.py}} --std--> run.log
	subgraph python 3.9 
	twitter.sh --execute--> id1{{twitter.py}}
	end
```



##### Connections info

| name          | value                                              |
| ------------- | -------------------------------------------------- |
| shell script  | [twitterNotify.sh](script/shell/twitterNotify.md)  |
| python script | [twitterNotify.py](script/python/twitterNotify.md) |
| Databases     | [client.db](database/database_template.md)         |



## Script Information

| Details           | Data                      |
| ----------------- | ------------------------- |
| **Projects**      | Notification of Invoices. |
| **Project Lead**  | arc986                    |
| **Provider**      | arc986                    |
| **SOA Architect** | arc986                    |



## API Changes History

| Branch                                          | Date       | Project | Responsible | Comment |
| ----------------------------------------------- | ---------- | ------- | ----------- | ------- |
| [main](https://gitlab.com/arc986examen/twitter) | 28/06/2021 | twitter | arc986      | RC      |
|                                                 |            |         |             |         |
|                                                 |            |         |             |         |
|                                                 |            |         |             |         |
