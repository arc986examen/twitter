## Data Base Name


#### Relation Diagram 
```mermaid
classDiagram

      message  "1" --> "N"  customer
      
      resume  "JOIN" --> "JOIN"  customer
      resume  "JOIN" --> "JOIN"  message
      
      class message{
          *id
          message()
      }
      class customer{
          *id
          +message_id
          twitter()
          completeName()
          ammount()
          sendMessage()
      }
      
      class resume{
          *id
          message()
          twitter()
          completeName()
          ammount()
          sendMessage()
      }
```
#### Tables

| name                            | type  | Details                                                      |
| ------------------------------- | ----- | ------------------------------------------------------------ |
| [/message](tables/message.md)   | table | new table, stores the template of messages sent to customers. |
| [/customer](tables/customer.md) | table | new table, stores necessary customer information.            |
| [/resume](views/resume.md)      | view  | new view, centralizes the message and customer table, filters the data to be sent by day. |
