## Table customer


##### Description

stores necessary customer information.

##### Foreign Keys

| name       | owner    | refTable | type    |
| ---------- | -------- | -------- | ------- |
| message_id | customer | message  | INTEGER |



##### Indexes

| name | table    | unique |
| ---- | -------- | ------ |
| id   | customer | Y      |



##### Attributes

| Column | Type|null?|length| comment |
| ------ | ----| -----------| -----------|----------- |
| id | INTENER | N | -1 | id customers |
| message_id | INTENER | N | -1 | foreign key related to the configured message to clients |
| twitter | TEXT | N | 40 | twitter user without @ |
| completeName | TEXT | N | 40 | customer's full name |
| amount | REAL | N | -1 | amount owed from customer |
| sendMessage | TEXT | N | -1 | day when the messages should be sent |
