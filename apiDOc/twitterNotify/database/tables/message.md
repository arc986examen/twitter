## Table message


##### Description

stores the template of messages sent to customers.

##### Indexes

| name | table   | unique |
| ---- | ------- | ------ |
| id   | message | Y      |



##### Attributes


| Column | Type|null|length| comment |
| ------ | ----| -----------| -----------|----------- |
| id | integer | N | -1 | primary key |
| message | TEXT | N | 60 | configurable messages for clients |

