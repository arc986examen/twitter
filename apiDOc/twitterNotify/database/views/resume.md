## VIEW resume

###### VIEW

centralizes the message and customer table, filters the data to be sent by day.

```sql
CREATE VIEW resume AS
SELECT c.id, c.twitter, c.completeName, c.ammount, m.message , c.sendMessage
FROM customer c join message m on(c.message_id == m.id and strftime('%d', datetime('now','localtime')) == strftime('%d', c.sendMessage));
```
