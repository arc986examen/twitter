# twitterNotify [ python script ]

Describes the python script process.



## Method Logic<A>

```mermaid
graph LR
		id1((shell_script)) --> process
    process --> response
    response --> id1((shell_script))
```

## Process sequence diagram<AD>
```mermaid
sequenceDiagram
    python_script->>twiter_biz_api: [POST][body] /directMessages [user,message]
    twiter_biz_api-->>python_script: id message or error
```



### Services dependencies

This section defines all the connections to the web services and the methods that are used within the method.

###### twitter-biz-api

| Method<A>       | Type<A> |
| --------------- | ------- |
| /directMessages | POST    |
