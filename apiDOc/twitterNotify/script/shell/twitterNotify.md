# twitterNotify [ shell script ]

<The purpose of the method is described.>



## Method Logic<A>

```mermaid
graph LR
		id1((cronJob)) --> process
    process --> response
    response --> id1((cronJob))
```

## Process sequence diagram<AD>
```mermaid
sequenceDiagram
    twitterNotify.sh->>twitterNotify.py: Execute
    twitterNotify.py-->>twitterNotify.sh: Response
    twitterNotify.sh->>run.log: Response
```



### Script dependencies

This section defines all the executions.

###### python script

| Method<A>     | Type<A> |
| ------------- | ------- |
| twitterNotify | SCRIPT  |
