# twitter-biz-api
Service with the objective of communication between the business and customers using the social network twitter 


##### Connections Diagram
```mermaid
graph TD
	id0((client)) --post--> /Tweets
	id0((client)) --post--> /directMessages
	id1{{twitter_biz_api}} --rest api--> twitter_sys_api
	subgraph Mule 4.3 
	/Tweets --request--> id1{{twitter_biz_api}}
	/directMessages --request--> id1{{twitter_biz_api}}
	end
```



##### Connections info

| name                   | value                                                        |
| ---------------------- | ------------------------------------------------------------ |
| Protocol               | http                                                         |
| Namespace              | localhost                                                    |
| Host                   | 127.0.0.1                                                    |
| Port                   | 8082                                                         |
| Path                   | /biz/api/                                                    |
| URL RAML Design Center | [RAML](https://anypoint.mulesoft.com/designcenter/designer/#/project/78f5b47c-e965-4352-978a-1be58d79d660) |
| URL RAML Exchange      | [RAML](https://anypoint.mulesoft.com/exchange/c9a04513-883e-49cd-8edc-82e12a94fe8d/twitter-biz-api/minor/0.0/) |
| Postman                | [Collection](postman/)                                       |
| Methods                | /directMessages [ [POST](methods/directMessages/POST/method.md) ] |
|                        | /Tweets [ [POST](methods/Tweets/POST/method.md) ]            |



## Service Information

| Details           | Data                      |
| ----------------- | ------------------------- |
| **Projects**      | Notification of Invoices. |
| **Project Lead**  | arc986                    |
| **Provider**      | arc986                    |
| **SOA Architect** | arc986                    |



## API Changes History

| Branch                                          | Date       | Project | Responsible | Comment |
| ----------------------------------------------- | ---------- | ------- | ----------- | ------- |
| [main](https://gitlab.com/arc986examen/twitter) | 28/06/2021 | twitter | arc986      | RC      |
|                                                 |            |         |             |         |
|                                                 |            |         |             |         |
|                                                 |            |         |             |         |
