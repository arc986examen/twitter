# /Tweets [ POST ]

used to post tweets to the gutter dashboard 



## Method Logic<A>

```mermaid
graph LR
		id1((client)) --> request
    request --> process
    process --> response
    response --> id1((client))
```

## Process sequence diagram<AD>
```mermaid
sequenceDiagram
	client->>twitter_biz_api: [POST][body] /Tweets [message]
    twitter_biz_api->>twitter_sys_api: [POST][body] /Tweets [message]
    twitter_sys_api-->>twitter_biz_api: tweet info or errors
    twitter_biz_api-->>client: tweet info or errors
```

## Request

This section defines all the possible data structures sent by the client when consuming the method. 



### URL

```http
http://localhost:8082/biz/api/Tweets
```



### Body

```json
{
  "message":"Tweet."
} 
```

###### Definitions

Each of the request parameters is detailed.

| name    | type   | description                               | required |
| ------- | ------ | ----------------------------------------- | -------- |
| message | String | message to be published on the dashboard. | Y        |



## Response 

In this section all the possible data structures received by the client at the moment of responding the method are defined. 



### Possible response success

This section defines all the possible data structures received by the client and that must be considered satisfactory at the time of responding to the method. 



#### [ 200 ]

OK

```json
 {
  "inReplyToUserId": -1,
  "userMentionEntities": [
    
  ],
  "source": "data",
  "retweeted": false,
  "currentUserRetweetId": -1,
  "createdAt": "data",
  "hashtagEntities": [
    
  ],
  "mediaEntities": [
    
  ],
  "inReplyToStatusId": -1,
  "retweetedStatus": null,
  "quotedStatusPermalink": null,
  "place": null,
  "text": "data",
  "id": 1,
  "lang": "data",
  "favorited": false,
  "displayTextRangeEnd": 1,
  "retweet": false,
  "accessLevel": 1,
  "URLEntities": [
    
  ],
  "truncated": false,
  "displayTextRangeStart": 1,
  "quotedStatusId": -1,
  "inReplyToScreenName": null,
  "rateLimitStatus": null,
  "possiblySensitive": false,
  "geoLocation": null,
  "quotedStatus": null,
  "retweetedByMe": false,
  "contributors": [
    
  ],
  "scopes": null,
  "symbolEntities": [
    
  ],
  "user": {
    "miniProfileImageURL": "data",
    "contributorsEnabled": false,
    "screenName": "data",
    "profileUseBackgroundImage": true,
    "profileBackgroundTiled": false,
    "createdAt": "data",
    "profileBackgroundImageURL": null,
    "biggerProfileImageURL": "data",
    "protected": false,
    "miniProfileImageURLHttps": "data",
    "profileImageURLHttps": "data",
    "id": 1,
    "profileImageURL": "data",
    "profileBanner600x200URL": null,
    "profileSidebarBorderColor": "data",
    "utcOffset": -1,
    "URLEntity": {
      "start": 1,
      "expandedURL": "data",
      "URL": "data",
      "displayURL": "data",
      "end": 1,
      "text": "data"
    },
    "timeZone": null,
    "defaultProfile": true,
    "profileLinkColor": "data",
    "profileBanner1500x500URL": null,
    "400x400ProfileImageURLHttps": "data",
    "originalProfileImageURLHttps": "data",
    "profileSidebarFillColor": "data",
    "rateLimitStatus": null,
    "profileBannerMobileURL": null,
    "name": "data",
    "followersCount": 1,
    "followRequestSent": false,
    "status": null,
    "showAllInlineMedia": false,
    "geoEnabled": false,
    "profileBannerMobileRetinaURL": null,
    "translator": false,
    "profileTextColor": "data",
    "description": "data",
    "descriptionURLEntities": [
      
    ],
    "URL": null,
    "biggerProfileImageURLHttps": "data",
    "profileBannerIPadURL": null,
    "lang": null,
    "email": null,
    "defaultProfileImage": false,
    "statusesCount": 1,
    "profileBanner300x100URL": null,
    "accessLevel": 1,
    "profileBackgroundImageUrlHttps": null,
    "verified": false,
    "profileBackgroundColor": "data",
    "favouritesCount": 1,
    "friendsCount": 1,
    "listedCount": 1,
    "profileBannerURL": null,
    "location": "data",
    "400x400ProfileImageURL": "data",
    "profileBannerRetinaURL": null,
    "withheldInCountries": [
      
    ],
    "profileBannerIPadRetinaURL": null,
    "originalProfileImageURL": "data"
  },
  "retweetCount": 1,
  "withheldInCountries": null,
  "favoriteCount": 1
} 
```

###### Definitions

Each of the request parameters is detailed.

| name | type | description | required |
| ---- | ---- | ----------- | -------- |
|      |      |             | <Y or N> |



### Possible response error

In this section all the possible data structures received by the client are defined and that must be considered as unsatisfactory when responding to the method.

#### [ 304 ]

Not Modified

```json
 {
  "code": 304,
  "message": "Not Modified"
} 
```



#### [ 400 ]

Bad Request

```json
{
    "code":400,
    "message": "Bad Request"
}
```



#### [ 401 ]

Unauthorized

```json
{
    "code":400,
    "message": "Unauthorized"
}
```



#### [ 403 ]

Forbidden

```json
{
    "code":403,
    "message": "Forbidden"
}
```



#### [ 404 ]

Not Found

```json
{
    "code":400,
    "message": "Not Found"
}
```



#### [ 406 ]

Not Acceptable

```json
{
    "code":406,
    "message": "Not Acceptable"
}
```



#### [ 410 ]

Gone

```json
{
    "code":410,
    "message": "Gone"
}
```



#### [ 422 ]

Unprocessable Entity

```json
{
    "code":422,
    "message": "Unprocessable Entity"
}
```



#### [ 429 ]

Too Many Requests

```json
{
    "code":429,
    "message": "Too Many Requests"
}
```



#### [ 500 ]

Internal Server Error

```json
{
    "code":500,
    "message": "Internal Server Error"
}
```



#### [ 502 ]

Bad Gateway

```json
{
    "code":502,
    "message": "Bad Gateway"
}
```



#### [ 503 ]

Service Unavailable

```json
{
    "code":503,
    "message": "Service Unavailable"
}
```



#### [ 504 ]

Gateway Timeout

```json
{
    "code":504,
    "message": "Gateway Timeout"
}
```



## Administration and data management 

In this section you define all the transformations, temporary and final repositories of the data within the method flow.



### Transformation Request <A>

In this section the matrix of all the data transformations that is carried out within the service is defined. 

| Original Payload<A> | Mulesoft<D> | <destiny> transformation<A> |
| :------------------ | :---------- | :-------------------------- |
| message             | message     | twitter-sys-api/Tweets      |



### Services dependencies

This section defines all the connections to the web services and the methods that are used within the method.

###### twitter-sys-api

| Method<A> | Type<A> |
| --------- | ------- |
| /Tweets   | POST    |

