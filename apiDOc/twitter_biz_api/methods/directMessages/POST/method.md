# /directMessages [ POST ]

it is used to send direct messages to the followers of the account 



## Method Logic<A>

```mermaid
graph LR
		id1((client)) --> request
    request --> process
    process --> response
    response --> id1((client))
```

## Process sequence diagram<AD>
```mermaid
sequenceDiagram
	client->>twitter_biz_api: [POST][body] /directMessages [user,message]
    twitter_biz_api->>twitter_sys_api: [GET][queryParam] /user/search [user]
    twitter_sys_api-->>twitter_biz_api: userId and other or errors
    alt if search get userId
    twitter_biz_api->>twitter_sys_api: [POST][body] /directMessages [userId,message]
    twitter_sys_api-->>twitter_biz_api: id message or errors
    end
    twitter_biz_api-->>client: id message or error
```

## Request

This section defines all the possible data structures sent by the client when consuming the method. 



### URL

```http
http://localhost:8082/biz/api/directMessages
```



### Body

```json
{
  "user": "arc986",
  "message":"Tweet."
} 
```

###### Definitions

Each of the request parameters is detailed.

| name    | type   | description                      | required |
| ------- | ------ | -------------------------------- | -------- |
| user    | String | Twitter user.                    | Y        |
| message | String | message to send to twitter user. | Y        |



## Response 

In this section all the possible data structures received by the client at the moment of responding the method are defined. 



### Possible response success

This section defines all the possible data structures received by the client and that must be considered satisfactory at the time of responding to the method. 



#### [ 200 ]

OK

```json
{
  "event": {
    "id": 12345
  }
} 
```

###### Definitions

Each of the request parameters is detailed.

| name     | type   | description                               | required |
| -------- | ------ | ----------------------------------------- | -------- |
| event    | object | structure that describes the events event | Y        |
| event/id | long   | event identifier                          | Y        |



### Possible response error

In this section all the possible data structures received by the client are defined and that must be considered as unsatisfactory when responding to the method.

#### [ 304 ]

Not Modified

```json
 {
  "code": 304,
  "message": "Not Modified"
} 
```



#### [ 400 ]

Bad Request

```json
{
    "code":400,
    "message": "Bad Request"
}
```



#### [ 401 ]

Unauthorized

```json
{
    "code":400,
    "message": "Unauthorized"
}
```



#### [ 403 ]

Forbidden

```json
{
    "code":403,
    "message": "Forbidden"
}
```



#### [ 404 ]

Not Found

```json
{
    "code":400,
    "message": "Not Found"
}
```



#### [ 406 ]

Not Acceptable

```json
{
    "code":406,
    "message": "Not Acceptable"
}
```



#### [ 410 ]

Gone

```json
{
    "code":410,
    "message": "Gone"
}
```



#### [ 422 ]

Unprocessable Entity

```json
{
    "code":422,
    "message": "Unprocessable Entity"
}
```



#### [ 429 ]

Too Many Requests

```json
{
    "code":429,
    "message": "Too Many Requests"
}
```



#### [ 500 ]

Internal Server Error

```json
{
    "code":500,
    "message": "Internal Server Error"
}
```



#### [ 502 ]

Bad Gateway

```json
{
    "code":502,
    "message": "Bad Gateway"
}
```



#### [ 503 ]

Service Unavailable

```json
{
    "code":503,
    "message": "Service Unavailable"
}
```



#### [ 504 ]

Gateway Timeout

```json
{
    "code":504,
    "message": "Gateway Timeout"
}
```



## Administration and data management 

In this section you define all the transformations, temporary and final repositories of the data within the method flow.



### Transformation Request <A>

In this section the matrix of all the data transformations that is carried out within the service is defined. 

| Original Payload<A> | Mulesoft<D> | <destiny> transformation<A>    |
| :------------------ | :---------- | :----------------------------- |
| user                | user        | twitter-sys-api/user/search    |
|                     | userId      | twitter-sys-api/directMessages |
| message             | message     | twitter-sys-api/directMessages |



### Services dependencies

This section defines all the connections to the web services and the methods that are used within the method.

###### twitter-sys-api

| Method<A>       | Type<A> |
| --------------- | ------- |
| /user/search    | GET     |
| /directMessages | POST    |

