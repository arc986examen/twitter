# twitter-sys-api
Service with the objective of communication between the business and customers using the social network twitter 


##### Connections Diagram
```mermaid
graph TD
	id0((client)) --post--> /Tweets
	id0((client)) --post--> /User/search
	id0((client)) --post--> /directMessages
	id1{{twitter_sys_api}} --rest api--> twitter_api
	subgraph mule 4.3
	/User/search --request--> id1{{twitter_sys_api}}
	/Tweets --request--> id1{{twitter_sys_api}}
	/directMessages --request--> id1{{twitter_sys_api}}
	end
```



##### Connections info

| name                   | value                                                        |
| ---------------------- | ------------------------------------------------------------ |
| Protocol               | http                                                         |
| Namespace              | localhost                                                    |
| Host                   | 127.0.0.1                                                    |
| Port                   | 8081                                                         |
| Path                   | /sys/api/                                                    |
| URL RAML Design Center | [RAML](https://anypoint.mulesoft.com/designcenter/designer/#/project/536c98e7-67f9-4bbf-bfec-2116cb252297) |
| URL RAML Exchange      | [RAML](https://anypoint.mulesoft.com/exchange/c9a04513-883e-49cd-8edc-82e12a94fe8d/twitter-sys-api/minor/0.0/) |
| Postman                | [Collection](postman/)                                       |
| Methods                | /User/search [ [GET](methods/User_search/GET/method.md) ]    |
|                        | /directMessages [ [POST](methods/directMessages/POST/method.md) ] |
|                        | /Tweets [ [POST](methods/Tweets/POST/method.md) ]            |



## Service Information

| Details           | Data                      |
| ----------------- | ------------------------- |
| **Projects**      | Notification of Invoices. |
| **Project Lead**  | arc986                    |
| **Provider**      | arc986                    |
| **SOA Architect** | arc986                    |



## API Changes History

| Branch                                          | Date       | Project | Responsible | Comment |
| ----------------------------------------------- | ---------- | ------- | ----------- | ------- |
| [main](https://gitlab.com/arc986examen/twitter) | 28/06/2021 | twitter | arc986      | RC      |
|                                                 |            |         |             |         |
|                                                 |            |         |             |         |
|                                                 |            |         |             |         |
