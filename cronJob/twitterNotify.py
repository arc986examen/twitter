import sqlite3
import requests

url = 'http://localhost:8082/biz/api/directMessages'
con = sqlite3.connect('client.db')
con.row_factory = sqlite3.Row
cur = con.cursor()
cur.execute("SELECT id, twitter, completeName, ammount, message, sendMessage FROM resume;")
for row in cur.fetchall():
    json = {"user": row['twitter'], "message": row['completeName']+' '+row['message']+str(row['ammount'])}
    response = requests.post(url, json=json)
    print("\n-START---------------------------------------------------------------------------\n")
    print("REQUEST\n")
    print(json)
    print("\nRESPONSE\n")
    print("--STATUS CODE\n")
    print(str(response.status_code))
    print("\n--BODY\n")
    print(str(response.json()))
    print("\n-END-----------------------------------------------------------------------------\n")
con.close()
