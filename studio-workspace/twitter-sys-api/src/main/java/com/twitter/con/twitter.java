package com.twitter.con;

import twitter4j.User;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.Status;

public class twitter {

	static public ConfigurationBuilder init(String Consumer_Key, String Consumer_Secret, String Access_Token, String Access_Token_Secret) throws TwitterException {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey(Consumer_Key).setOAuthConsumerSecret(Consumer_Secret).setOAuthAccessToken(Access_Token).setOAuthAccessTokenSecret(Access_Token_Secret);
		return cb;
	}

	static public Status Tweet(ConfigurationBuilder cb, String Message) throws TwitterException {
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		return twitter.updateStatus(Message);
	}

	static public ResponseList<User> UserSearch(ConfigurationBuilder cb, String User) throws TwitterException {
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		return twitter.searchUsers(User, 1);
	}

	static public long DirectMessage(ConfigurationBuilder cb, long UserId, String Message) throws TwitterException {
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		DirectMessage message = twitter.sendDirectMessage(UserId, Message);
		return message.getId();
	}
}
